void main() {
  percabanganSederhana();
  print("");
  percabanganBersarang();
  print("");
  switchCase();
}

//if dan else
void percabanganSederhana() {
  var tomat = "buah";
  if (tomat == "sayur") {
    print("Tomat merupakan sayur");
  } else {
    print("Tomat merupakan " + tomat);
  }
}

//if, else if, dan else
void percabanganBersarang() {
  var total = 120000;
  if (total == 100000) {
    print("Selamat anda mendapatkan bonus gelas");
  } else if (total > 100000 && total < 150000) {
    print("Selamat anda mendapatkan bonus piring");
  } else if (total >= 150000) {
    print("Selamat anda mendapatkan bonus termos");
  } else {
    print("Belanja lebih banyak lagi agar mendapatkan bonus menarik");
  }
}

// switch case
void switchCase() {
  var buttonPushed = 1;
  switch (buttonPushed) {
    case 1:
      {
        print('Nyalakan TV!');
        break;
      }
    case 2:
      {
        print('Turunkan volume TV!');
        break;
      }
    case 3:
      {
        print('Tingkatkan volume TV!');
        break;
      }
    case 4:
      {
        print('Matikan TV!');
        break;
      }
    default:
      {
        print('Tidak terjadi apa-apa');
      }
  }
}

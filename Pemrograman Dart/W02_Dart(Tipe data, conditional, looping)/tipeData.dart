void main() {
  tipeList();
}

void tipeString(){
  var nama = "Fikry Maulana Muzahidin";
  var prodi = "Teknik Informatika";
  var umur = 19;

  print("Nama :  " + nama);
  print("Prodi : " + prodi);
  print(nama.runtimeType); //mengecek tipe data
  print(nama.contains("Maulana")); //mengecek string tertentu
  print(nama.substring(13, 16)); //tampil berdasar (start, finish) 
  print(nama.length); //panjang data
  print(nama.indexOf("lana"));  //menecek posisi index
  print(prodi.toUpperCase()); //huruf besar semua
  print(prodi.toLowerCase()); //huruf kecil semua
  print(prodi.isEmpty); //apakah variabel bernilai kosong
  print(prodi.isNotEmpty); //apakah variabel tidak kosong
  print("Umur : " + umur.toString()); //mengubah tipe data  integer ke string
}

void tipeNumber() {
  var berat1 = 45;
  var berat2 = 50.99;

  print("Berat: " +
      berat1.toString() +
      " dan $berat2"); // ada 2 cara pemanggilan, ubah ke string atau langsung kedalam string
  print(berat1.runtimeType);
  print(berat2.runtimeType);
  print(berat2.toString().runtimeType); // merubah ke string
  print(berat2.ceil()); // membulatkan ke atas
  print(berat2.floor()); // membulatkan ke bawah
}

void tipeList() {
  List<int> data = [50, 30, 80, 10, 70];
  print(data);
  print(data[1]); // tampilkan index tertentu
  print(data.elementAt(2)); // tampilkan index tertentu
  print(data.length); // panjang data

  //menambah data
  data.add(20);
  print(data);
}




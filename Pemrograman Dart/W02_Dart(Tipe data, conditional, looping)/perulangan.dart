void main() {
  print("WHILE LOOP");
  whileLoop();
  print("");
  print("FOR LOOP");
  forLoop();
}

//while
void whileLoop() {
  var siswa = 1;
  while (siswa < 6) {
    print("Siswa ke- " + siswa.toString());
    siswa++; // Mengubah nilai siswa dengan menambahkan 1
  }
}

//for
void forLoop() {
  var jumlah = 0;
  for (var deret = 5; deret > 0; deret--) {
    jumlah += deret;
    print('Jumlah saat ini: ' + jumlah.toString());
  }
  print('Jumlah terakhir: ' + jumlah.toString());
}

void main() {
  var nilai1 = 20;
  var nilai2 = 6;

  print("");
  print("Operator Aritmatika");
  print(nilai1 + nilai2);
  print(nilai1 - nilai2);
  print(nilai1 * nilai2);
  print(nilai1 / nilai2);
  print(nilai1 % nilai2);
  print("");

  print("Operator Perbandingan");
  print(nilai1 > nilai2);
  print(nilai1 < nilai2);
  print(nilai1 >= nilai2);
  print(nilai1 <= nilai2);
  print(nilai1 == nilai2);
  print(nilai1 != nilai2);
}
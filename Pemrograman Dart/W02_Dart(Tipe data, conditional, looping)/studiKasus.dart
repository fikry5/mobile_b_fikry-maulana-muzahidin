import 'dart:io';

void main() {
  print("Kasus 1");
  kasus1();
  print("");
  kasus2();
  print("");
  print("Kasus 3");
  kasus3();
  print("");
  print("Kasus 4");
  kasus4();

}

void kasus1() {
  print("Apakah anda akan menginstall Dart? Y/T");
  var respon = stdin.readLineSync();

  respon == "Y"
      ? print("Anda akan menginstall aplikasi dart")
      : respon == "T"
          ? print("Aborted")
          : print("Gagal");
}

void kasus2() {
  print("Masukkan Nama Anda");
  // Membaca nama
  var nama = stdin.readLineSync();

  if (nama == "") {
    print("Nama harus diisi");
  } else {
    print(
        "Pilih Peran Anda, ketikkan angka? \n1. Penyihir \n2. Guard \n3. Werewolf");
    var peran = stdin.readLineSync();

    if (peran == "") {
      print("Hallo, $nama Pilih peranmu untuk memulai game!");
    } else if (peran == "1") {
      print(
          "Selamat datang di Dunia Werewolf, $nama! \nHalo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == "2") {
      print(
          "Selamat datang di Dunia Werewolf, $nama! \nHalo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if (peran == "3") {
      print("Selamat datang di Dunia Werewolf, $nama"
          "\nHalo Werewolf $nama, Kamu akan memakan mangsa setiap malam!");
    } else {
      print("Tidak ada yang dipilih");
    }
  }
}


void kasus3() {
  print("Pilih hari (Senin-Minggu) :  ");
  var hari = stdin.readLineSync();
  switch (hari) {
    case "Senin":
      {
        print(
            "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
        break;
      }
    case "Selasa":
      {
        print(
            "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
        break;
      }
    case "Rabu":
      {
        print(
            "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.");
        break;
      }
    case "Kamis":
      {
        print(
            "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
        break;
      }
    case "Jum'at":
      {
        print("Hidup tak selamanya tentang pacar.");
        break;
      }
    case "Sabtu":
      {
        print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
        break;
      }
    case "Minggu":{
        print(
            "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");
        break;}
    default:{
        print("None");}
  }
}

void kasus4() {
  //for loop
  for (int i = 1; i < 21; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
      print("I Love Coding");
    } else if (i % 2 == 0) {
      print("Berkualitas");
    } else if (i % 2 == 1) {
      print("Santai");
    }
  }
}

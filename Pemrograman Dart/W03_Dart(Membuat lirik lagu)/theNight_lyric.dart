void main(List<String> args) async {
  print("\n The Night (Lyric) - Avicii");
  print("-----------------------------");
  print(await baris1());
  print(await baris2());
  print(await baris3());
  print(await baris4());
  print(await baris5());

}

Future<String> baris1() async {
  String lirik = "He said: One day you'll leave this world behind";
  return await Future.delayed(Duration(seconds: 3), () => (lirik));
}

Future<String> baris2() async {
  String lirik = "So live a life you will remember";
  return await Future.delayed(Duration(seconds: 7), () => (lirik));
}

Future<String> baris3() async {
  String lirik = "My father told me when I was just a child";
  return await Future.delayed(Duration(seconds: 11), () => (lirik));
}

Future<String> baris4() async {
  String lirik = "These are the nights that never die";
  return await Future.delayed(Duration(seconds: 15), () => (lirik));
}

Future<String> baris5() async {
  String lirik = "My father told me \n";
  return await Future.delayed(Duration(seconds: 19), () => (lirik));
}